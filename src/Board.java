public class Board {
    private int[][] blocks;
    private int n;

    // construct a board from an n-by-n array of blocks
    // (where blocks[i][j] = block in row i, column j)
    public Board(int[][] blocks) {
        this.n = blocks.length;
        this.blocks = blocks;
    }

    // board dimension n
    public int dimension() {

        return this.n;
    }

    // number of blocks out of place
    public int hamming() {


    }

    // sum of Manhattan distances between blocks and goal
    public int manhattan() {


    }

    // is this board the goal board?
    public boolean isGoal() {

    }

    // a board that is obtained by exchanging any pair of blocks
    public Board twin() {

    }

    // does this board equal y?
    public boolean equals(Object y) {

    }

    // all neighboring boards
    public Iterable<Board> neighbors() {

    }

    // string representation of this board (in the output format specified below)
    public String toString() {

        StringBuilder str = new StringBuilder();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                str.append(this.blocks[i][j]);
            }
            str.append("/n");
        }

        return str.toString();
    }

    // unit tests (not graded)
    public static void main(String[] args) {

    }
}